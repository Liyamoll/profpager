/* mytest.c:
 *
 *  Test C to assembly interface 
 */

#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include "common.h"
#include "stm32f3xx.h"
#include "stm32f3_discovery.h"
#include "nrf24l01.h"
#include "mytest.h"

/* Globals */
uint8_t sta;
int i=0;
unsigned char rx_buf[TX_PLOAD_WIDTH];

int mytest( int x );

void CmdTest(int action)
{
  if(action==CMD_SHORT_HELP) return;
  if(action==CMD_LONG_HELP) 
   {
     printf("testasm\n\n"
	   "This command tests the C to assembler interface\n"
	   );
     return;
  }
  printf("ret val = %d\n", mytest( 77  ) );
}
ADD_CMD("cmdtest",CmdTest,"Test C to asm")

void Cmdkey(int mode)
{
int rc = 0;
 if(mode != CMD_INTERACTIVE) {
 return;
 }
rc = get_key();
printf("%c pressed\n",rc);
}
ADD_CMD("keypad", Cmdkey,"                Read keypad");


/* Command Initialization */ 
void CmdInit(int mode)      
{
  if(mode != CMD_INTERACTIVE) return;
  GPIOInit();
  SPIInit(); 
  GPIOD_Init();
  RFread();
}
ADD_CMD("Initialize",CmdInit,"  Initialization ");

/* command to send */ 
void sendData(int mode)      
{
  if(mode != CMD_INTERACTIVE) return;
  TX_test();
}
ADD_CMD("Test1",sendData,"  transmission");

/* command to send */ 
void sendData2(int mode)      
{
  if(mode != CMD_INTERACTIVE) return;
  TX_test2();
}
ADD_CMD("Test2",sendData2,"  transmission");

/* command to recieve */ 
void RecieveData(int mode)      
{
  if(mode != CMD_INTERACTIVE) return;
  RFread();
}
ADD_CMD("read",RecieveData,"  transmission");


/* Transmitting section */
void TX_test() 
{
  unsigned char Message1[] = "Calling Mike";
  SPI3_readWriteReg(FLUSH_TX,0); // flushes transmitter
  TX_Mode();  //sets in tx mode
  SPI3_writeBuf(WR_TX_PLOAD, Message1, TX_PLOAD_WIDTH);
  SPI3_readWriteReg(RF_WRITE_REG+STATUS,(SPI3_readReg(RF_READ_REG+STATUS))); 
  SetTimer(0,500);               // sets timer 0 for 500 micro seconds.
}

void TX_test2() 
{
  unsigned char Rudy[] = {"Calling Rudy Hofer"};
  SPI3_readWriteReg(FLUSH_TX,0);        //Flushing the transmitter
  TX_Mode();
  SPI3_writeBuf(WR_TX_PLOAD, Rudy, TX_PLOAD_WIDTH);
  SPI3_readWriteReg(RF_WRITE_REG+STATUS,(SPI3_readReg(RF_READ_REG+STATUS)));    //0x07  'Status' register address
  SetTimer(0,500);               // sets timer 0 for 500 micro seconds.
}

//<<<<<<< HEAD
void sendMsg(char address, char msgCode)
{
  unsigned char Message[2] = {address, msgCode};
  SPI3_readWriteReg(FLUSH_TX,0);        //Flushing the transmitter
  SPI3_writeBuf(WR_TX_PLOAD, Message, TX_PLOAD_WIDTH);
  SPI3_readWriteReg(RF_WRITE_REG+STATUS,(SPI3_readReg(RF_READ_REG+STATUS)));    //0x07  'Status' register address
  SetTimer(0,500);               // sets timer 0 for 500 micro seconds.
}

/* Recieving station */


unsigned char* RFread()
{
  if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13))  //checks for IRQ=0, indicating a message was recieved
                   return 0;
  sta = SPI3_readReg(RF_READ_REG+STATUS);
  SPI3_readWriteReg(RF_WRITE_REG+STATUS,sta);
  if (RX_DR)
     {
      SPI3_readBuf(RD_RX_PLOAD, rx_buf, RX_PLOAD_WIDTH);
      SPI3_readWriteReg(FLUSH_RX,0);
      //BSP_LED_On(2);
     /* for (i=0;i<12;i++)
      {
      printf("%c",rx_buf[i]);
      }*/
     return rx_buf;

     }
  else
    return NULL;
 }
