/* headers */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "stm32f3xx_hal.h"
#include "stm32f3_discovery.h"
#include "common.h"
#include "LCD.h"

/*Data Register*/
uint8_t *Register = (uint8_t *) 0x48000c14 + 0x01;

/*Declaration Of GPIO Pins*/
uint32_t DataGPIO[8] = {GPIO_PIN_8, GPIO_PIN_9, GPIO_PIN_10, GPIO_PIN_11, GPIO_PIN_12, GPIO_PIN_13, GPIO_PIN_14, GPIO_PIN_15};

/*Initialization of GPIOPins*/
int32_t GPIOD_Init() 
{
 __GPIOD_CLK_ENABLE();
 GPIO_InitTypeDef gpio_init_struct; 
/*GPIO_PIN_4 FOR Register select|GPIO_3 For R/W|GPIO_5 For Enable|GPIO(0TO7) For Data*/ 
 gpio_init_struct.Pin = GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | DataGPIO[0] | DataGPIO[1] | DataGPIO[2] | DataGPIO[3] | DataGPIO[4] |   DataGPIO[5] | DataGPIO[6] | DataGPIO[7];

/*Initialization Of Gpio's Data Pins Of Family D*/
 gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
 gpio_init_struct.Pull = GPIO_PULLDOWN;
 gpio_init_struct.Speed = GPIO_SPEED_LOW;
 HAL_GPIO_Init(GPIOD, &gpio_init_struct);
 LCD_Write_Command(0x38);  /*Command For Blink LCD's Cursor And Home Command*/
 LCD_Write_Command(0x0F);  /*Command For Increment The Cursor*/
 LCD_Write_Command(0x06);		
 return 0;
}

/*Function For GPIO Pin8_B*/
void GPIO_Pin8_B_Enable()
{
/*Initialization Of Gpio's Pins Of Family B*/
__GPIOB_CLK_ENABLE();
GPIO_InitTypeDef gpio_init_struct; 
gpio_init_struct.Pin       = GPIO_PIN_8;
gpio_init_struct.Mode      = GPIO_MODE_OUTPUT_PP;
gpio_init_struct.Pull      = GPIO_NOPULL;
gpio_init_struct.Speed     = GPIO_SPEED_LOW;
gpio_init_struct.Alternate = 0;
HAL_GPIO_Init(GPIOB, &gpio_init_struct);
HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, 1);
}

void GPIO_Pin8_B_Disable()
{
/*Initialization Of Gpio's Pins Of Family B*/
__GPIOB_CLK_ENABLE();
GPIO_InitTypeDef gpio_init_struct; 
gpio_init_struct.Pin       = GPIO_PIN_8;
gpio_init_struct.Mode      = GPIO_MODE_OUTPUT_PP;
gpio_init_struct.Pull      = GPIO_NOPULL;
gpio_init_struct.Speed     = GPIO_SPEED_LOW;
gpio_init_struct.Alternate = 0;
HAL_GPIO_Init(GPIOB, &gpio_init_struct);
HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, 0);
}

/*Function For Initialization Of LCD*/
void LCD_Initialization_Cmd(int mode)
{
GPIOD_Init() ;
} 
ADD_CMD("LCDINIT", LCD_Initialization_Cmd, "	Initialize Of The LCD GPIOs");


/*Function For Write Command*/
void LCD_Write_Command( uint8_t cmd_add )
{
 *Register = cmd_add;
 HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, 0);                       //Disable Read Write pin
 HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4, 0);                       //Disable Register Select Pin
 HAL_Delay(1);
 HAL_GPIO_WritePin(GPIOD, GPIO_PIN_5, 1);                       //Make Enable Pin High
 HAL_Delay(1);
 HAL_GPIO_WritePin(GPIOD, GPIO_PIN_5, 0);                       //Make Enable Pin Low
 HAL_Delay(1);
}

/*Function For LCD Data Command*/
void LCD_Command_Data( uint8_t cmd_add )
{
 *Register = cmd_add;
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, 0);                        //Disable Read Write pin
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4, 1);                        //Enable Register Select Pin
  HAL_Delay(1);
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_5, 1);                        //Make Enable Pin High
  HAL_Delay(1);
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_5, 0);                        //Make Enable Pin Low
  HAL_Delay(1);
}

/*Function For Home Command*/
void LCD_HOME( void )
{
  LCD_Write_Command(0x80);
}


/*Function for Clear Screen Of LCD*/
void LCD_CLR_SCR( void )
{
 LCD_Write_Command(0x01);
}

void setCursor(unsigned char x, unsigned char y) 
{
  unsigned char copy_y = 0;
  if (x > (16 - 1))
       x = 0;
  if (y > (2 - 1))
       y = 0;
 
switch (y) {
	case 0:
		copy_y = 0x80;
		break;
	case 1:
		copy_y = 0xc0;
		break;
	case 2:
		copy_y = 0x94;
		break;
	case 3:
		copy_y = 0xd4;
		break;
	}
	LCD_Write_Command(x + copy_y);
}

/*Function For String On LCD */
void LCD_PUT_S( char *string )
{
  int i = 0;
  for ( i=0;string[i] != '\0';i++ )
   {
     LCD_Command_Data(string[i]);
   }
}

void LCD_Data_Command(int mode)
{
  setCursor(4,1);
  LCD_PUT_S("ProfPAGER");

}
ADD_CMD("LCDDATA", LCD_Data_Command, "Send Data Command On LCD");


