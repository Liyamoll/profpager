#ifndef __LCD_H
#define __LCD_H

int32_t GPIOD_Init(void);
void GPIO_Pin8_B_Enable(void);
void GPIO_Pin8_B_Disable(void);
void LCD_CLR_SCR( void );
void setCursor(unsigned char x, unsigned char y);
void LCD_PUT_S( char *string );

#endif 
