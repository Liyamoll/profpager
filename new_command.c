/* new_command.c:
 *
 *    Example command file.
 *
 */

#include <stdint.h>
#include <stdio.h>

#include "common.h"

void CmdHello(int mode)
{

  if(mode != CMD_INTERACTIVE) return;

  printf("Hello World\n");

  return;
}

ADD_CMD("hello",CmdHello,"                Print Hello")
