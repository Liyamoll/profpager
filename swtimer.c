/* swtimer.c:
 *
 *  Implementation of software timers with callbacks
 */

#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include "common.h"

/* Timer flags */
#define TIMER_ALLOCATED  (1<<0)
#define TIMER_COUNTING   (1<<1)
#define TIMER_REPEATING  (1<<2)

typedef struct Timer_s {
  uint32_t flags;
  uint32_t count;
  uint32_t setCount;
  uint32_t expiredCount;
  void     *data;
  void     (*callback)(void *);
  char     *name;
} Timer_t;

/* Local storage */
static Timer_t timers[NUM_SWTIMERS];

/* Initialize timers */
void SWTimerInit(void)
{
  /* Zero out the whole timer array */
  memset(timers, 0, sizeof(timers));

}


/* Allocate a timer from the pool */
uint32_t SWTimerAllocate(uint32_t *handle, void (*callback)(void *),
			 void *data, char *name)
{
  uint32_t i;
  /* Find a free timer */
  for(i=0; i<NUM_SWTIMERS; i++) {
    if(!(timers[i].flags & TIMER_ALLOCATED)) {
      timers[i].flags        = TIMER_ALLOCATED;
      timers[i].callback     = callback;
      timers[i].data         = data;
      timers[i].name         = name;
      timers[i].expiredCount = 0;
      timers[i].setCount     = 0;
      *handle = i;
      return 0;
    }
  }
  /* No free timers */
  return 1;
}

/* Return a timer to the pool */
uint32_t SWTimerFree(uint32_t handle)
{
  if(handle >= NUM_SWTIMERS) {
    printf("Invalid handle %u passed to SWTimerFree(), "
	   "max value allowed is %d\n", (unsigned int)handle, NUM_SWTIMERS);
    return 1;
  }
  /* Is this timer actually allocated? */
  if(!(timers[handle].flags & TIMER_ALLOCATED)) {
    printf("Timer %u is not allocated, can't free it!\n",
	   (unsigned int)handle);
    return 1;
  }

  memset(&timers[handle],0,sizeof(timers[0]));
  return 0;
}

uint32_t SWTimerSet(uint32_t handle, uint32_t count)
{
  if(handle >= NUM_SWTIMERS) {
    printf("Invalid handle %u passed to SWTimerSet(), "
	   "max value allowed is %d\n", (unsigned int)handle, NUM_SWTIMERS);
    return 1;
  }
  /* Is this timer actually allocated? */
  if(!(timers[handle].flags & TIMER_ALLOCATED)) {
    printf("Timer %u is not allocated, can't set it!\n",
	   (unsigned int)handle);
    return 1;
  }

  timers[handle].count    = count;
  timers[handle].setCount = count;
  /* If count is 0, then we are cancelling the timer */
  if(count) {
    timers[handle].flags |= TIMER_COUNTING;
  } else {
    timers[handle].flags &= ~TIMER_COUNTING;
  }
  return 0;
}

uint32_t SWTimerSetData(uint32_t handle, void *data)
{
  if(handle >= NUM_SWTIMERS) {
    printf("Invalid handle %u passed to SWTimerSet(), "
	   "max value allowed is %d\n", (unsigned int)handle, NUM_SWTIMERS);
    return 1;
  }
  /* Is this timer actually allocated? */
  if(!(timers[handle].flags & TIMER_ALLOCATED)) {
    printf("Timer %u is not allocated, can't set it!\n",(unsigned int)handle);
    return 1;
  }

  timers[handle].data = data;
  return 0;
}

uint32_t SWTimerSetRepeating(uint32_t handle)
{
  if(handle >= NUM_SWTIMERS) {
    printf("Invalid handle %u passed to SWTimerSet(), "
	   "max value allowed is %d\n", (unsigned int)handle, NUM_SWTIMERS);
    return 1;
  }
  /* Is this timer actually allocated? */
  if(!(timers[handle].flags & TIMER_ALLOCATED)) {
    printf("Timer %u is not allocated, can't set it!\n",(unsigned int)handle);
    return 1;
  }

  timers[handle].flags |= TIMER_REPEATING;
  return 0;
}

uint32_t SWTimerExpired(uint32_t handle)
{
  uint32_t expired;
  if(handle >= NUM_SWTIMERS) {
    printf("Invalid handle %u passed to SWTimerExpired(), "
	   "max value allowed is %d\n", (unsigned int)handle, NUM_SWTIMERS);
    return 0;
  }
  /* Is this timer actually allocated? */
  if(!(timers[handle].flags & TIMER_ALLOCATED)) {
    printf("Timer %u is not allocated, don't know if it is counting!\n",
	   (unsigned int)handle);
    return 0;
  }

  /* Is it a repeater, if so, return the number of counts */
  if(timers[handle].flags & TIMER_REPEATING) {
    expired = timers[handle].expiredCount;
    timers[handle].expiredCount = 0;
    return expired;
  }
  /* Is it counting? */
  if(!(timers[handle].flags & TIMER_COUNTING)) {
    /* No, then it is expired */
    return 1;
  }
  return 0;
}

/* Tick all the timers, and do callbacks */
void SWTimerTick(void)
{
  uint32_t i;
  for(i=0; i<NUM_SWTIMERS; i++) {
    if((timers[i].flags & TIMER_COUNTING)
       && (timers[i].count)) {
      /* Timer is active, decrement count */
      timers[i].count--;
      if(timers[i].count == 0) {
	if(timers[i].flags & TIMER_REPEATING) {
	  /* Repeating counter, reload */
	  timers[i].count = timers[i].setCount;
	  timers[i].expiredCount++;
	} else {
	  /* non-repeating timer has expired */
	  timers[i].flags &= ~TIMER_COUNTING;
	}
	if(timers[i].callback != NULL) {
	  timers[i].callback(timers[i].data);
	}
      }
    }
  }
}

static void TimerCallBack(void *data)
{
  printf("Timer Expired\n");
  SWTimerFree((uint32_t)data);
}

void CmdTestTimer(int mode)
{
  uint32_t count, handle, rc;

  if(mode != CMD_INTERACTIVE) return;

  count = 5000;
  fetch_uint32_arg(&count);
  
  /* Allocate a timer */
  rc = SWTimerAllocate(&handle,TimerCallBack,NULL,"Test Timer");
  if(rc) {
    printf("Unable to allocate a timer\n");
    return;
  }

  /* Fill the data field with the handle */
  rc = SWTimerSetData(handle, (void *)handle);
  if(rc) {
    printf("Unable to set timer data\n");
    SWTimerFree(handle);
    return;
  }

  /* Set the timer */
  rc = SWTimerSet(handle,count);
  if(rc) {
    printf("Unable to set timer\n");
    SWTimerFree(handle);
    return;
  }

  printf("Timer set\n");
  return;
}
ADD_CMD("testtimer",CmdTestTimer,"{count}         Test SW Timer"); 

void CmdDumpTimers(int mode)
{
  uint32_t i;

  if(mode != CMD_INTERACTIVE) return;

  for(i=0; i<NUM_SWTIMERS; i++) {
    printf("%3u: %7u 0x%08x %9s %8s %9s %s\n",
	   (unsigned int)i, 
	   (unsigned int)timers[i].count,
	   (unsigned int)timers[i].data,
	   timers[i].flags & TIMER_ALLOCATED ? "ALLOCATED" : "",
	   timers[i].flags & TIMER_COUNTING  ? "COUNTING" : "",
	   timers[i].flags & TIMER_REPEATING ? "REPEATING" : "",
	   timers[i].name != NULL ? timers[i].name : "");
  }
  return;
}
ADD_CMD("dumptimers",CmdDumpTimers,"                Dump SW Timers");


void SetTimer(uint32_t timer,uint32_t val)
{
  uint32_t handle;
  SWTimerAllocate(&handle,TimerCallBack,NULL,"Set Timer");
  SWTimerSet(timer,val); 
  SWTimerSetData(handle, (void *)handle);
  printf("Timer set\n");	
  return;
}


