
/* reciever.c:
 *
 */
#include <stdint.h>
#include <stdio.h>

#include "common.h"
#include "LCD.h"
//#include "memory.h"
#include "keypad.h"
#include "mytest.h"
#include "nrf24l01.h"
#include "buzzer.h"
/* Define reciever states */
#define DEF_ENUM(enum)   enum,
typedef enum RecieverState_e {
#include "reciever.inc"
  MAX_STATE
} RecieverState_t;
#undef DEF_ENUM

/* Create state code to text mapping */
#define DEF_ENUM(enum)   #enum,
static const char *const RecieverStateName[MAX_STATE] = {
  #include "reciever.inc"
};
#undef DEF_ENUM

typedef enum RecieverFlags_e {
  MAX_FLAGS
} RecieverFlags_t;


/* local variables */
static RecieverState_t RecieverCurrentState, RecieverPreviousState;
static uint32_t Flags;
int rc;
void Buzzer();
 
/* Local functions */
static const char *RecieverGetStateName(RecieverState_t s)
{
  if(s < MAX_STATE) {
    return RecieverStateName[s];
  }
  return NULL;
}

void sendcode(char msgCode)
{
   if(msgCode == 0x31)
   {
      LCD_PUT_S("Wait ");
      setCursor(0,1);
      LCD_PUT_S("5 minutes");
   }
   else if(msgCode == 0x32)
   {
      LCD_PUT_S("Sorry,");
      setCursor(0,1);
      LCD_PUT_S("busy right now");
   }
   else if(msgCode == 0x23)
   {
      LCD_PUT_S("Sorry,");
      setCursor(0,1);
      LCD_PUT_S("Unavailable");
   }
   else 
   {
      LCD_PUT_S("Error");
   }
}

static void TimerCallBack(void *data)
{
   return;
}


/* Dump the current Reciever state */
void RecieverDumpState(void)
{
  printf("Reciever: %s, flags:0x%04x\n",
	 RecieverGetStateName(RecieverCurrentState),
	 (unsigned)Flags);
}

/* Initialize Reciever module */
void RecieverInit(void)
{
  Flags = 0;
  RecieverPreviousState = Reset;
  RecieverCurrentState  = Idle;
  uint32_t handle = 0;
  rc = SWTimerAllocate(&handle,TimerCallBack,NULL,"Test Timer");
    if(rc) {
      printf("Unable to allocate a timer\n");
      return;
    }
    /* Fill the data field with the handle */
    rc = SWTimerSetData(handle, (void *)handle);
    if(rc) {
      printf("Unable to set timer data\n");
      SWTimerFree(handle);
      return;
    }
    GPIOD_Init();
    GPIOInit();//sets up pins
    SPIInit();//sets up SPI

}

#define ON_ENTRY if(RecieverCurrentState != RecieverPreviousState)
   
void RecieverTask(void)
{
  RecieverState_t nextState;
  static char address = NULL;
  static char key = NULL;
  static unsigned char* msg = NULL;
  int handle = 0;
  int count = 2000;

  /* Stay in the current state by default */
  nextState = RecieverCurrentState;
  switch(RecieverCurrentState) {
  case Idle:
    ON_ENTRY {}
    nextState = Reset;
    break;
  case Reset:
    ON_ENTRY {LCD_CLR_SCR();}
    nextState = Start;
    address = getAddress();
    break;
  case Start:
    ON_ENTRY {LCD_CLR_SCR();
    RX_Mode();}
    msg = RFread();
    if (msg!=NULL && msg[0] == address)//if we got a message adressed to this controller
    {
	nextState = Alert;
    }
    else if (msg!=NULL)//if we got a message adressed to another controller
    {
	nextState = DisplayReciever;
    }
    break;
  case Alert:
    ON_ENTRY {LCD_CLR_SCR();
    setCursor(0,0);
    LCD_PUT_S(getName(address));
    setCursor(0,1);
    LCD_PUT_S("press 1/2/#");   // 1 ->  ComeIn, 2-> Wait 5 Minutes, # -> Unavailable
    buzzerOn();
    //RX_Mode();
     //set up expire timer
    
    /* Set the timer */
    rc = SWTimerSet(handle,count);
    if(rc) {
      printf("Unable to set timer\n");
      SWTimerFree(handle);
      return;
    }}
    key = get_key();
    msg = RFread();
    if(key == 0X31|| key == 0X32 || key == 0X23) 
    {
       nextState = SendResponse;
       buzzerOff();
    }
    else if(msg!=NULL && msg[1] == 0x33) //if another reciever says they are unavailable
    {
       nextState = Start;
       buzzerOff();
    }
     else if(SWTimerExpired(handle)) //if no response for 15s
    {
       nextState = Expire;
       buzzerOff();
    }
    break;
  case DisplayReciever:
    ON_ENTRY {LCD_CLR_SCR();
    setCursor(0,0);
    LCD_PUT_S("Call for");
    LCD_PUT_S(getName(msg[0]));
    setCursor(0,1);
    LCD_PUT_S(" # for unavailable");
    buzzerOn();
    
    /* Set the timer */
    rc = SWTimerSet(handle,count);
    if(rc) {
      printf("Unable to set timer\n");
      SWTimerFree(handle);
      return;
    }}
   
    key = get_key();
    if (key == 0x23)//if unavailable is pressed
    {
	nextState = SendResponse;
        buzzerOff();
    }
    else if (SWTimerExpired(handle))
    {
	nextState = Start; //for now, no need to display another person's call expiring
        buzzerOff();
    }
    break;
  case SendResponse:
    ON_ENTRY {LCD_CLR_SCR();
    LCD_PUT_S("Sent:");
    sendcode(key);
    TX_Mode();
    sendMsg(key,00);
    /* Set the timer */
    rc = SWTimerSet(handle,count);
    if(rc) {
      printf("Unable to set timer\n");
      SWTimerFree(handle);
      return;
    }}
    //wait long enough for professor to read message before going back
    if (SWTimerExpired(handle))
    {
	nextState = Start;
    }
    break;
  case Expire:
    ON_ENTRY {LCD_CLR_SCR();
    LCD_PUT_S("Call expired");
   // RX_Mode();
    /* Set the timer */
    rc = SWTimerSet(handle,count);
    if(rc) {
      printf("Unable to set timer\n");
      SWTimerFree(handle);
      return;
    }}
    //wait long enough for professor to read message before going back
    if (SWTimerExpired(handle))
    {
	nextState = Start;
    }
    break;
  case MAX_STATE:
    printf("Error: Reciever Invalid state %u!!!!\n",RecieverCurrentState);
  }

  /* Save the Current state in case we change it */
  RecieverPreviousState = RecieverCurrentState;

  /* Is a state change pending? */
  if(nextState != RecieverCurrentState) {
    /* Log it */
    printf(":RecieverState %s -> %s\n",
	   RecieverGetStateName(RecieverCurrentState),
	   RecieverGetStateName(nextState));
    /* Update the state variable */
    RecieverCurrentState = nextState;
  }
}
