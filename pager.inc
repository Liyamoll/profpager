#ifndef DEF_ENUM
#define DEF_ENUM(x)
#endif  
DEF_ENUM(Idle)  
DEF_ENUM(Reset)
DEF_ENUM(Start)
DEF_ENUM(ConfirmOrCancel)
DEF_ENUM(Call)
DEF_ENUM(Expire)
DEF_ENUM(DisplayResponse)
#undef DEF_ENUM
