#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "stm32f3xx_hal.h"
#include "stm32f3_discovery.h"
#include "common.h" 
#include "nrf24l01.h"
#include "keypad.h"

void delay (unsigned int); // prototype
/* DELAY FNCTION */
void delay (unsigned int j)
{
 unsigned int k,l;
 for(k=0;k<j;k++)
 for(l=0;l<1000;l++);
}


char get_key (void) 
{

 RCC->AHBENR |= 1 << 19; 	// Enable GPIOC clock	
 RCC->AHBENR |= 0x00020000;	// Enable GPIOA clock
 GPIOC->MODER |= 0x1000;		// configure PC6 for output 
 GPIOA->MODER |= 0x00010100;		// Configure PA8 and PA4 for output
/* CONFIGURING FOR SECOND COLUMN*/
 GPIOA->BSRRH = 0X10;		// reset PC4 = 0
 GPIOC->BSRRL = 0X40;		// set PC6 = 1
 GPIOA->BSRRL = 0X100;           // set pA8 =1
 if (!(GPIOC->IDR & 0x08)) 
  {
   delay (3);
   if (!(GPIOC->IDR & 0x08))
    {
      
      return 0X31; // return ASCII code of 1
    }
  }
 if (!(GPIOC->IDR & 0x04)) 
  {
   delay (3);
   if (!(GPIOC->IDR & 0x04))
     return 0x34; // return ASCII code of 4
  }
 if (!(GPIOC->IDR & 0x02)) 
   {
    delay (3);
    if (!(GPIOC->IDR & 0x02))  
       return 0X37; // return ASCII code of 7   
   }
 if (!(GPIOC->IDR & 0x01)) {
   delay (3);
   if (!(GPIOC->IDR & 0x01))
   return 0x2A; // return ASCII code of *
   }

/* CONFIGURING FOR SECOND COLUMN*/
 GPIOA->BSRRH = 0X100;		// reset PA5 = 0
 GPIOC->BSRRL = 0X40;		// set PC6 = 1
 GPIOA->BSRRL = 0X10;		// set PA4 = 1

 if (!(GPIOC->IDR & 0x08)) 
 {
  delay (10);
 if (!(GPIOC->IDR & 0x08))
    {
     
      return 0x32; // return ASCII code of 2
    }
 }
 if (!(GPIOC->IDR & 0x01)) 
 {
  delay (10);
 if (!(GPIOC->IDR & 0x01))
  return 0x30; // return ASCII code of 0
 }
 if (!(GPIOC->IDR & 0x02)) 
 {
  delay (10);
 if (!(GPIOC->IDR & 0x02))
  return 0x38; // return ASCII code of 8
 }
 if (!(GPIOC->IDR & 0x04)) 
 {
  delay (10);
 if (!(GPIOC->IDR & 0x04))
  return 0x35; // return ASCII code of 5
 }
 /* CONFIGURING FOR THIRD COLUMN*/
  GPIOC->BSRRH = 0X40;		// reset PC6 = 0
  GPIOA->BSRRL = 0X120;		// set PA8,Pa4 = 1

 if (!(GPIOC->IDR & 0x01))
 {
  delay (10);
 if (!(GPIOC->IDR & 0x01))
  return 0x23; // return ASCII code of #
 }

 if (!(GPIOC->IDR & 0x02)) 
 {
  delay (10);
 if (!(GPIOC->IDR & 0x02))
  return 0x39; // return ASCII code of 9
 }

if (!(GPIOC->IDR & 0x04)) {
delay (10);
if (!(GPIOC->IDR & 0x04))
return 0x36; // return ASCII code of 6
}

 if (!(GPIOC->IDR & 0x08)) 
 {
  delay (10);
 if (!(GPIOC->IDR & 0x08))
  return 0x33; // return ASCII code of 3
 }
  return 0x00;

}

