/* common.h:
 *
 *  Common include file
 */
#ifndef __COMMON_H
#define __COMON_H

#include <time.h>


/* Baud rate for serial port */
#define UARTBAUDRATE     38400
//#define UARTBAUDRATE     9600

//#define USE_UART     1
#define USE_USB      1

#define USE_USB_INTERRUPT_DEFAULT   1


/* How many system ticks per second */
#define TICK_RATE       (1000)
#define MILLISECONDS(x) ((x*1000)/TICK_RATE)
#define SECONDS(x)      ((x)*TICK_RATE)
#define MINUTES(x)      ((x)*SECONDS(60))
#define HOURS_T(x)      ((x)*MINUTES(60))

#define NUM_SWTIMERS  20


/* definitions */
#define FALSE 0
#define TRUE  1

/* I/O's */

/*
 *  Build version information
 */
extern const time_t VersionBuildDate;
extern const char VersionBuildUser[];


/*
 *  Simple table driven parser for a command line interface.
 *
 */

#define CMD_INTERACTIVE   0
#define CMD_SHORT_HELP    1
#define CMD_LONG_HELP     2

typedef struct {
  char *cmdname;       /* String containing the name of the command */
  void (*func)(int);   /* Pointer to the action function */
  char *help;          /* Help string for the command */
} parse_table;

#define ADD_CMD(name,f,helptxt) \
const parse_table f##E __attribute__ ((section(".parsetable." name))) = { \
    .cmdname = name,  \
    .func    = f, \
    .help    = helptxt };

void TaskInput(void);
int parse(char *buf, int len,
	  const parse_table *table);  /* Parse the buffer and call commands */ 

int fetch_int32_arg(int32_t *dest);      /* Fetch an integer argument */
int fetch_uint32_arg(uint32_t *dest);  /* Fetch a UWORD argument */
int fetch_double_arg(double *dest);   /* Fetch a floating point argument */
int fetch_float_arg(float *dest);     /* Fetch a floating point argument */
int fetch_string_arg(char **dest); /* Fetch a string argument */
int fetch_cmd_args(char **dest);      /* Fetch the rest of the command */

/*
 *  Dumping functions
 */
void DumpBuffer(uint8_t *buffer, uint32_t count, uint32_t address);
void CmdDump(int action);

/*
 * terminal.c functions 
 */


typedef enum {
#ifdef USE_UART
  INDEX_UART,
#endif
#ifdef USE_USB
  INDEX_USB,
#endif
  INDEX_MAX } PortIndex_e;


void TerminalInit(void);
uint32_t TerminalRead(uint32_t index, uint8_t *ptr, uint32_t len);
int TerminalReadNonBlock(uint32_t index,char *c);
int TerminalReadAnyNonBlock(char *c);
uint32_t TerminalInputBufferWrite(uint32_t index, char *p, uint32_t len);

/* Master parameter database */
typedef struct Parameters_s {
  /* Do the bit type parameters first */
#define PARAM_BIT(t,s,n,d,h)   t n : s;
  #include "parameters.inc"
  /* Everything else after, as adding bits usually doesn't shift the
   * rest of the structure.
   */
#define PARAM(w,x,y,z)   w x;
#define PARAM_ARRAY(t,s,n,d,h)   t n[s];
#define PARAM_ARRAY2(t,s,n,d,h)   t n[s];
#include "parameters.inc"
} Parameters_t;

extern Parameters_t Params;
/* Parameter functions */
void ParameterInit(void);
uint32_t ParameterLoad(uint32_t force);
uint32_t ParameterSave(void);
void ParameterList(FILE *fp);
char* getName(char id);
char getAddress();

/* Software timer functions */
#define ALLOC_TIMER(name) do {			 \
  rc = SWTimerAllocate(& name, NULL, NULL, #name); \
  if(rc) {                                       \
    printf("Failed to allocate " #name "\n");    \
  }} while(0)

#define ALLOC_TIMER_CB(name,cb) do {		   \
  rc = SWTimerAllocate(& name, cb, NULL, #name); \
  if(rc) {                                       \
    printf("Failed to allocate " #name "\n");    \
  }} while(0)

void SWTimerInit(void);
uint32_t SWTimerAllocate(uint32_t *handle, void (*callback)(void *), void *data, char *name);
uint32_t SWTimerFree(uint32_t handle);
uint32_t SWTimerSet(uint32_t handle, uint32_t count);
uint32_t SWTimerSetData(uint32_t handle, void *data);
uint32_t SWTimerSetRepeating(uint32_t handle);
uint32_t SWTimerExpired(uint32_t handle);
void SWTimerTick(void);
void SetTimer(uint32_t timer,uint32_t val);

/*
 * main.c functions
 */
void Error_Handler(void);

/* mycode.s functions, and variables */
void my_Loop( void );
void my_Init( void );
void my_Tick( void );
extern volatile uint32_t myTickCount;


/* LCD display functions */
void LCD_PUT_S( char *string );
void LCD_CLR_SCR( void );
int32_t GPIOD_Init(void);
void LCD_Write_Command( uint8_t cmd_add );

/* keypad functions */

char get_key (void);



#endif
