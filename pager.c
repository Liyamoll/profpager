
/* pager.c:
 *
 */
#include <stdint.h>
#include <stdio.h>

#include "common.h"
#include "LCD.h"
//#include "memory.h"
#include "keypad.h"
#include "mytest.h"
#include "nrf24l01.h"

/* Define pager states */
#define DEF_ENUM(enum)   enum,
typedef enum PagerState_e {
#include "pager.inc"
  MAX_STATE
} PagerState_t;
#undef DEF_ENUM

/* Create state code to text mapping */
#define DEF_ENUM(enum)   #enum,
static const char *const PagerStateName[MAX_STATE] = {
  #include "pager.inc"
};
#undef DEF_ENUM

typedef enum PagerFlags_e {
  MAX_FLAGS
} PagerFlags_t;


/* local variables */
static PagerState_t PagerCurrentState, PagerPreviousState;
static uint32_t Flags;
int rc;

 
/* Local functions */
static const char *PagerGetStateName(PagerState_t s)
{
  if(s < MAX_STATE) {
    return PagerStateName[s];
  }
  return NULL;
}

void response(char msgCode)
{
   if(msgCode == 0x31)
   {
      LCD_PUT_S("Wait");
      setCursor(0,1);
      LCD_PUT_S("5 Minutes");
   }
   else if(msgCode == 0x32)
   {
      LCD_PUT_S("Sorry,busy");
      setCursor(0,1);
      LCD_PUT_S(" right now");
   }
   else if(msgCode == 0x23)
   {
      LCD_PUT_S("Sorry,");
      setCursor(0,1);
      LCD_PUT_S("Unavailable");
   }
   else 
   {
      LCD_PUT_S("Error\n");
   }
}

static void TimerCallBack(void *data)
{
   return;
}


/* Dump the current Pager state */
void PagerDumpState(void)
{
  printf("Pager: %s, flags:0x%04x\n",
	 PagerGetStateName(PagerCurrentState),
	 (unsigned)Flags);
}

/* Initialize pager module */
void PagerInit(void)
{
  Flags = 0;
  PagerPreviousState = Reset;
  PagerCurrentState  = Idle;
  uint32_t handle = 0;
  rc = SWTimerAllocate(&handle,TimerCallBack,NULL,"Test Timer");
    if(rc) {
      printf("Unable to allocate a timer\n");
      return;
    }
    /* Fill the data field with the handle */
    rc = SWTimerSetData(handle, (void *)handle);
    if(rc) {
      printf("Unable to set timer data\n");
      SWTimerFree(handle);
      return;
    }
    GPIOD_Init();
    GPIOInit();//sets up pins
    SPIInit();//sets up SPI

}

#define ON_ENTRY if(PagerCurrentState != PagerPreviousState)
   
void PagerTask(void)
{
  PagerState_t nextState;
  static char prof = NULL;
  char key = NULL;
  static unsigned char* msg = NULL;
  int handle = 0;
  int count = 2000;

  /* Stay in the current state by default */
  nextState = PagerCurrentState;
  switch(PagerCurrentState) {
  case Idle:
    ON_ENTRY {}
    nextState = Reset;
    break;
  case Reset:
    ON_ENTRY {LCD_CLR_SCR();}
    nextState = Start;
    break;
  case Start:
    ON_ENTRY {LCD_CLR_SCR();
    LCD_PUT_S("Select Professor");
    TX_Mode();}
    prof = get_key();
    if(prof != 0x00)//if a key was pressed
    {
       nextState = ConfirmOrCancel;
    }
    break;
  case ConfirmOrCancel:
    ON_ENTRY {LCD_CLR_SCR();
    setCursor(0,0);
    LCD_PUT_S(getName(prof));
    setCursor(0,1);
    LCD_PUT_S("* OK");
    setCursor(8,1);
    LCD_PUT_S("# CANCEL");
    TX_Mode();}
    key = get_key();
    if(key == 0x2A) //'*'
    {
       nextState = Call;
    }
    else if(key == 0x23) //'#'
    {
       nextState = Start;
    }
    break;
  case Call:
    ON_ENTRY {LCD_CLR_SCR();
    LCD_PUT_S("Dialling...");
    TX_Mode();
    sendMsg(prof,00);
//set up expire timer
    RX_Mode();   
    /* Set the timer */
    rc = SWTimerSet(handle,count);
    if(rc) {
      printf("Unable to set timer\n");
      SWTimerFree(handle);
      return;
    }}
    msg = RFread();
    if (msg!=NULL)//if we got a message adressed to this controller
    {
	nextState = DisplayResponse;
    }
    else if (SWTimerExpired(handle))
    {
	nextState = Expire;
    }
    break;
  case Expire:
    ON_ENTRY {LCD_CLR_SCR();
    LCD_PUT_S("Call expired,");
    setCursor(0,1);
    LCD_PUT_S("try again later");
    /* Set the timer */
    rc = SWTimerSet(handle,count);
    if(rc) {
      printf("Unable to set timer\n");
      SWTimerFree(handle);
      return;
    }}
    //wait long enough for student to read message before going back
    if (SWTimerExpired(handle))
    {
	nextState = Start;
    }
    break;
  case DisplayResponse:
    ON_ENTRY {LCD_CLR_SCR();
    response(msg[0]);
    /* Set the timer */
    rc = SWTimerSet(handle,count);
    if(rc) {
      printf("Unable to set timer\n");
      SWTimerFree(handle);
      return;
    }}
    //wait long enough for student to read message before going back
    if (SWTimerExpired(handle))
    {
	nextState = Start;
    }
    break;
  case MAX_STATE:
    printf("Error: Pager Invalid state %u!!!!\n",PagerCurrentState);
  }

  /* Save the Current state in case we change it */
  PagerPreviousState = PagerCurrentState;

  /* Is a state change pending? */
  if(nextState != PagerCurrentState) {
    /* Log it */
    printf(":PagerState %s -> %s\n",
	   PagerGetStateName(PagerCurrentState),
	   PagerGetStateName(nextState));
    /* Update the state variable */
    PagerCurrentState = nextState;
  }
}
