/* Parameters.inc:  -*-c-*-
 * 
 *  Parameter template file.  Define global editable parameters here.
 *
 *   Use the CATEGORY macro to define a new category.  Use the PARAM
 *   macro to define a parameter.  The four arguments are the data
 *   type, parameter name, default value, and the description.
 *
 */
#ifndef CATEGORY
#define CATEGORY(x)
#endif
/* PARAM(data_type, parameter_name, default_value, "description") */
#ifndef PARAM
#define PARAM(...)
#endif
/* PARAM_BIT(data_type, size, parameter_name, default_value, "description") */
#ifndef PARAM_BIT
#define PARAM_BIT(...)
#endif
/* PARAM_ARRAY(data_type, size, parameter_name, default_value, "description") */
#ifndef PARAM_ARRAY
#define PARAM_ARRAY(...)
#endif
/* Same as above, but with type specific get/set/print functions */
#ifndef PARAM_ARRAY2
#define PARAM_ARRAY2(...)
#endif

CATEGORY("This is a Category")
PARAM_ARRAY(char,20,string,"default","Help text")
PARAM_BIT(uint32_t,1,bitFlag,0,"Bit flag parameter on/off")
PARAM(uint32_t,uint32,0,"test uint32_t parameter")
PARAM(uint16_t,uint16,0,"test uint16_t parameter")
PARAM(uint8_t, uint8, 0,"test uint8_t parameter")
PARAM(uint8_t,address, 0, "pager address")
PARAM_ARRAY(char,20, name1, "default","first professor's name")
PARAM_ARRAY(char,20, name2, "default","second professor's name")
PARAM_ARRAY(char,20, name3, "default","third professor's name")
PARAM_ARRAY(char,20, name4, "default","fourth professor's name")
PARAM_ARRAY(char,20, name5, "default","fifth professor's name")
PARAM_ARRAY(char,20, name6, "default","sixth professor's name")
PARAM_ARRAY(char,20, name7, "default","seventh professor's name")
PARAM_ARRAY(char,20, name8, "default","eighth professor's name")
PARAM_ARRAY(char,20, name9, "default","ninth professor's name")
PARAM(float,testFloat, 1.0,"Test Floating point parameter")
PARAM(double,testDouble, 1.0,"Test Double precision Floating point parameter")
#undef PARAM
#undef PARAM_BIT
#undef PARAM_ARRAY
#undef PARAM_ARRAY2
#undef CATEGORY
